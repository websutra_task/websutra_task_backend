const express = require('express')
const validator = require('validator')
const router = express.Router()


router.post('/', (req, res, next) => {
    const email = req.body.email
    if (email == null) {
        res.status(400).json({
            message: 'email required'
        })
    } else if (validator.isEmail(email)) {
        res.status(200).json({
            email: email,
            message: 'email validation pass'
        })
    } else {
        res.status(400).json({
            message: 'email validation fail'
        })
    }
})


module.exports = router